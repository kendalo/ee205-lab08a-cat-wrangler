///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08 - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Kendal Oya<kendalo@hawaii.edu>
/// @brief  Lab 08 - CatWrangler - EE 205 - Spr 2021
/// @date   12_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include "list.hpp"
#include <cstdlib>
#include <iostream>
#include <cassert>
   
Node* head = new Node();
Node* tail = new Node();

const bool DoubleLinkedList::empty() const {
   return head == nullptr;
}

void DoubleLinkedList::push_front( Node* newNode ){
   if ( newNode == nullptr)
      return ;
   if (head != nullptr) {
      newNode->next = head;
      head->prev = newNode;
      newNode->prev = nullptr;
      head = newNode;
   }
   else {
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }

}

Node* DoubleLinkedList::pop_front(){
   if (head==nullptr)
      return nullptr;
   
   if (head==tail){
      Node* tmp = head;
      head = nullptr;
      tail = nullptr;
      return tmp;
   }   
   else{
      Node* tmp = head;
      head = head->next;
      head->prev = nullptr;
      tmp->next = nullptr;
      return tmp;
   }   
}

Node* DoubleLinkedList::get_first() const{
   return head;
}

Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   if (head==nullptr)
      return nullptr;
   return currentNode->next;
}
   
unsigned int DoubleLinkedList::size() const{
   unsigned int size = 0;
   Node* tmp = head;
   while(tmp!=nullptr){
      size=size + 1;
      tmp=tmp->next;
   }
   return size;
}

Node* DoubleLinkedList::get_last() const{
   return tail;
}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const{
   if (head==nullptr)
      return nullptr;
   return currentNode->prev;

}

void DoubleLinkedList::push_back ( Node* newNode ){
   if (newNode == nullptr)
      return;
   if (tail != nullptr){
      tail->next = newNode;
      newNode->prev = tail;
      newNode->next = nullptr;
      tail = newNode;
   }
   else{
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
}

Node* DoubleLinkedList::pop_back() {
   if (tail==nullptr)
      return nullptr;
   
   if(head == tail) {
      Node* tmp = tail;
      head = nullptr;
      tail = nullptr;
      return tmp;
   }
   else{
      Node* temp = tail;
      tail=tail->prev;
      tail->next = nullptr;
      temp->prev = nullptr;
      return temp;
   }
}

bool DoubleLinkedList::isIn(Node* node ) const {
   Node* tmp = head;
   while (tmp!= nullptr){
      if (tmp==node)
         return true;
      tmp = tmp->next;
   }
   return false;
}

bool DoubleLinkedList::isBefore(Node* a, Node* b){
   Node* temp = head;

   while(temp != nullptr){
      if(a == temp){
         return true;
      }else if(b == temp){
         return false;
      }
      temp = temp->next;
   }
   return false;
}


void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode ) {
   if (head==nullptr && currentNode==nullptr) {
      push_front( newNode);
      return;
   }
   if ( currentNode != nullptr && head == nullptr)
      assert(false );
   if ( currentNode == nullptr && head != nullptr)
      assert(false );
   
   assert (currentNode!=nullptr && head!=nullptr);
   assert ( isIn( currentNode ));
   assert ( !isIn( newNode ));

   if (currentNode == tail) {
      push_back(newNode);
      return;
   }
   else{
      newNode->prev = currentNode;
      newNode->next = currentNode->next;
      currentNode->next->prev = newNode;
      currentNode->next = newNode;
      return;
   }
}

void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode) {
   if (head==nullptr){
      head=newNode;
      tail = newNode;
      return;   
   }
   if (currentNode==head){
      newNode->next = currentNode;
      newNode->prev = nullptr;
      currentNode->prev = newNode;
      head = newNode;
      return;
   }
   else{
      newNode->prev = currentNode->prev;
      newNode->next = currentNode;
      currentNode->prev = newNode; 
      return;
   }
}

void DoubleLinkedList::swap(Node* node1, Node* node2){
   //Check if both nodes are in the list
   assert(isIn(node1) && isIn(node2));

   
   if(node1==node2){
      return;
   }
   
   //If node1 is not to the left of node2
   if(!isBefore(node1,node2)){
      Node* temp = node1;
      node1 = node2;
      node2 = temp;
   }

   
   Node* n1_left = node1->prev;
   Node* n1_right = node1->next;
   Node* n2_left = node2->prev;
   Node* n2_right = node2->next;

   
   bool isAdjacent = (n1_right == node2);

   if(!isAdjacent){
      node1->prev = node2->prev;
   }else{
      node1->prev = node2;
   }
   
   if(node1 != head){
      n1_left->next  = node2;
      node2->prev = n1_left;
   }else{
      node2->prev = nullptr;
      head = node2;
   }

   if(!isAdjacent){
      node2->next = n1_right;
   }else{
      node2->next = node1;
   }

   if(node2 != tail){
      n2_right->prev = node1;
      node1->next = n2_right;
   }else{
      node1->next = nullptr;
      tail = node1;
   }

   if(!isAdjacent){
      n1_right->prev = node2;
      n2_left->next = node1;
   }

  validate(); 

}


bool DoubleLinkedList::validate() const{
   if (head == nullptr) {
      assert (tail == nullptr);
      assert (empty());
      assert (size()==0);
   } else {
      assert(tail!= nullptr);
      assert(!empty());
      assert(size()>0);
   }
   if (tail== nullptr) {
      assert (head== nullptr);
      assert (empty());
      assert (size()==0);
   } else {
      assert(head!= nullptr);
      assert(!empty());
      assert(size()>0);
   }
   if (head!= nullptr && tail == head) {
      assert(size()==1);
   }
   
   unsigned int fCount = 0;
   Node* currentNode = head;
   while( currentNode !=nullptr){
      fCount++;
      if (currentNode->next != nullptr)
         assert( currentNode->next->prev==currentNode);
      currentNode=currentNode->next;
   }
   assert( size() == fCount);
   
   unsigned int bCount = 0;
   currentNode = tail;
   while( currentNode !=nullptr){
      bCount++;
      if (currentNode->prev!= nullptr)
         assert( currentNode->prev->next==currentNode);
      currentNode=currentNode->prev;
   }
   assert( size() == bCount);

   return true;
}

const bool DoubleLinkedList::isSorted() const {
   if (size()<=1)
      return true;

   for( Node* i = head; i->next != nullptr; i= i->next) {
      if (*i > *i->next )
         return false;
   }
   return true;
}

void DoubleLinkedList::insertionSort(){
   if(size()<=1){
   
      return;
   }


   for(Node* i = head->next; i != nullptr; i = i->next){
      
      for(Node* p = i->prev; p != nullptr; p = p->prev){
         if(*p > *i){
            
            swap(p,i);
         }
      }
   }


   validate();
   return;
}

